import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [{
      path: '/',
      name: 'regex',
      component: () => import('@/components/regex')
    },
    {
      path: '/pdp/:skuCode',
      name: 'pdp',
      component: () => import('@/components/pdp')
    },
    {
      path: '/home',
      name: 'home',
      component: () => import('@/components/home')
    },
    {
      path: '/directives',
      name: 'directives',
      component: () => import('@/components/directives')
    },
    {
      path: '/router',
      name: 'router',
      component: () => import('@/components/router')
    },
    {
      path: '/exercise',
      name: 'exercise',
      component: () => import('@/components/exercise')
    },
    {
      path: '/mini',
      name: 'mini',
      component: () => import('@/components/mini')
    },
    {
      path: '/account',
      component: () => import('@/components/account'),
      children: [

        {
          path: 'login',
          name: 'login',
          component: () => import('@/components/login')
        },
        {
          path: 'register',
          name: 'register',
          component: () => import('@/components/register')
        },
        {path:'/', redirect: 'login'}
      ]
    },

  ]
})

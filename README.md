# 安装node.js  node -v  npm -v 检查环境
# 全局安装脚手架 vue-cli   npm install -g vue-cli
# 初始化项目 vue init webpack projectname
# 安装依赖  npm install 
# 启动 npm run dev


1. vue 基础语法
   1) data
   2) computed
   3) methods
   4) watch
   5) 生命周期  create mounted update destroyed
   6) 指令(v-if ... )
   7) 过滤器  组件通信  slot *
   8) 路由 * 

2. ES6 使用
   let  map find reduce ... async await  * 

3. element-ui  axios *   (官网通用 三方插件)

4. vuex  (组件状态管理) *

5. casaba 项目熟悉
